import pickle
import json
import random
import numpy as np
import torch
import copy
import pandas as pd

from utils.translator import translate

seed = 1626
np.random.seed(seed)
random.seed(seed)


def read_file(file_path):
    total_item = []
    with open(file_path, 'r', encoding='utf-8') as f:
        for line in f.readlines():
            translation = translate(line.strip())
            total_item.append(translation)
    return total_item


def translate_dict(data):
    if isinstance(data, dict):
        return {
            translate(k): translate_dict(v)
            for k, v in data.items()
        }
    elif isinstance(data, list):
        return [translate_dict(i) for i in data]
    else:
        return translate(data)


def get_disease_and_symptom(data_path, disease_path, symptom_path):
    with open(data_path, 'rb') as f:
        goal = pickle.load(f)

    total_disease = read_file(disease_path)
    total_sym = read_file(symptom_path)

    total_disease_dict = dict(zip(total_disease, [i for i in range(len(total_disease))]))
    disease_num = len(total_disease_dict)

    total_sym_dict = dict(zip(total_sym, [i+disease_num for i in range(len(total_sym))]))
    symptom_num = len(total_sym_dict)

    return goal, total_disease_dict, total_sym_dict, disease_num, symptom_num


def load_disease_sym_txt(path):
    line_list = []
    with open(path, 'r', encoding='utf-8') as f:
        for line in f.readlines():
            line_list.append(np.array(line.split(), dtype=float))
    return np.array(line_list)


def load_disease_sym_pk(path):
    with open(path, 'rb') as f:
        data = pickle.load(f)
    return data


def make_train_data(goals_set, total_disease_dict, total_sym_dict):
    symptom_num = len(total_sym_dict)
    disease_num = len(total_disease_dict)
    train_samples = np.full((len(goals_set), symptom_num + disease_num), -1)
    train_ans = []
    keys_list = []
    for i, dialog in enumerate(goals_set):
        disease = dialog['disease_tag']
        disease_index = total_disease_dict[disease]
        train_samples[i][disease_index] = 1
        train_ans.append(disease_index)
        for k, v in dialog['implicit_inform_slots'].items():
            v_index = total_sym_dict[k]
            if v:
                train_samples[i][v_index] = 1
        for k, v in dialog['explicit_inform_slots'].items():
            v_index = total_sym_dict[k]
            if v:
                train_samples[i][v_index] = 1
    return train_samples, train_ans

def make_test_data(goals_set, total_disease_dict, total_sym_dict):
    test_samples = []
    test_ans = []
    for i,dialog in enumerate(goals_set):
        one_evidence = {}
        disease = dialog['disease_tag']
        disease_index =  total_disease_dict[disease]
        test_ans.append(disease_index)
        for k, v in dialog['implicit_inform_slots'].items():
            v_index = total_sym_dict[k]
            if v:
                one_evidence[str(v_index)] = 1
            else:
                one_evidence[str(v_index)] = -1
        for k, v in dialog['explicit_inform_slots'].items():
            v_index = total_sym_dict[k]
            if v:
                one_evidence[str(v_index)] = 1
            else:
                one_evidence[str(v_index)] = -1
        test_samples.append(one_evidence)
    return test_samples, test_ans

def get_bayes_struct(goal, total_disease_dict, total_sym_dict, threshold=5):
    # disease_sym - Store the relationship between each disease and the other related symptoms. 存储每一个疾病与其他哪些症状有关系
    # sym_disease - Store which symptoms are related to each disease. 存储每一个症状与哪些疾病有关系
    # disease_num = len(total_disease_dict)
    sym_disease = {}
    disease_sym = {}
    # yan = np.zeros(disease_num, symptom_num)
    disease_sym_num = {}
    for dialog in goal['train']:
        disease = dialog['disease_tag']
        true_set = set()
        for symptom_type in ["implicit_inform_slots", "explicit_inform_slots"]:
            for k, v in dialog[symptom_type].items():
                if v:
                    true_set.add(k)
                    if k not in sym_disease.keys():
                        sym_disease[k] = set()
                    sym_disease[k].add(disease)
        if disease not in disease_sym.keys():
            disease_sym[disease] = set()
            disease_sym_num[disease] = {}
        for s in true_set:
            disease_sym_num[disease][s] = disease_sym_num[disease].get(s, 0) + 1
        disease_sym[disease].update(true_set)
    bayes_graph = []
    for k, v in disease_sym_num.items():
        disease = total_disease_dict[k]
        for kk, vv in disease_sym_num[k].items():
            # print(kk,vv)
            if vv >= threshold:
                bayes_graph.append(tuple([str(disease), str(total_sym_dict[kk])]))
    # print("bayes edges num: ", len(bayes_graph))  # 边的条数
    return bayes_graph

def get_dataset_information(data_path, disease_path, symptom_path, random_rate=1.0, edge_threshold=5):
    goal, total_disease_dict, total_sym_dict, disease_num, symptom_num = \
        get_disease_and_symptom(data_path, disease_path, symptom_path)   # 需要保留结构，只减少数据 Need to retain the structure, only reduce data.
    train_len = len(goal['train'])
    choice_index = random.sample(list(np.arange(train_len)), int(train_len * random_rate))
    train_samples, train_ans = make_train_data([goal['train'][i] for i in choice_index], total_disease_dict, total_sym_dict)
    test_samples, test_ans = make_test_data(goal['test'], total_disease_dict, total_sym_dict)
    bayes_data = pd.DataFrame(train_samples, columns=[str(i) for i in range(disease_num + symptom_num)])
    # train_sample_data = pd.DataFrame(train_samples[:, disease_num:], columns=[str(i + disease_num) for i in range(symptom_num)])
    # test_sample_data = pd.DataFrame(test_samples[:, disease_num:], columns=[str(i + disease_num) for i in range(symptom_num)])

    bayes_graph = get_bayes_struct(goal, total_disease_dict, total_sym_dict, edge_threshold)
    return goal, total_disease_dict, total_sym_dict, bayes_data, bayes_graph, test_samples, test_ans
