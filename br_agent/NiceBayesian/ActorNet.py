import torch
import numpy as np
from torch import nn
from typing import Any, Dict, Tuple, Union, Optional, Sequence
from tianshou.data import to_torch
from tianshou.utils.net.common import Net
import copy

from exceptions.exceptions import FeatureModelNotSupportedError


class MyActor(nn.Module):
    def __init__(
        self,
        args,
        preprocess_net: nn.Module,
        action_shape: Sequence[int],
        conditional_probability_matrix: np.array = None,
        mutual_information_matrix: np.array = None,
        prob_threshold: float = 0.95,
    ) -> None:
        super().__init__()
        self.action_shape = action_shape
        self.preprocess = preprocess_net
        self.conditional_matrix_weight = args.conditional_matrix_weight
        self.mutual_matrix_weight = args.mutual_matrix_weight

        self.feature_model = args.feature_model  # defaults to 4
        # 0 means using only the conditional_probability_matrix,
        # 2 means using only the mlp
        if self.feature_model == 1:  # 1 means using only the mutual_information_matrix,
            self.final_matrix = mutual_information_matrix
        elif self.feature_model == 3:  # 3 means integrating all three,
            if mutual_information_matrix is None:
                print("error, need mutual_information_matrix in actornet.py")
            self.final_matrix = (
                conditional_probability_matrix + mutual_information_matrix
            )
        elif self.feature_model >= 4:
            # 4 means using mlp to learn a parameter to adjust these two matrices.
            # 5 means to use simple weighted average of the two matrices
            # 6 means using only the RNN and not MLP
            self.final_matrix = conditional_probability_matrix
            self.mutual_information_matrix = nn.Parameter(
                torch.tensor(mutual_information_matrix, dtype=torch.float32),
                requires_grad=args.transfor_grad,
            )
        else:
            self.final_matrix = conditional_probability_matrix

        self.conditional_probability_matrix = nn.Parameter(
            torch.tensor(self.final_matrix, dtype=torch.float32),
            requires_grad=args.transfor_grad,
        )

        self.device = args.device
        self.max_turn = args.max_episode_steps
        self.threshold = prob_threshold
        self.disease_num = self.conditional_probability_matrix.size(0)
        self.symptom_num = self.conditional_probability_matrix.size(-1)
        self.mlp_net = None

        if self.feature_model >= 2:
            if self.feature_model == 4: # learn a beita
                self.mlp_net = Net(
                    layer_num=2,
                    state_shape=tuple(
                        [
                            self.disease_num + self.symptom_num,
                        ]
                    ),
                    action_shape=1,
                    softmax=False,
                )
            elif self.feature_model == 6:
                self.rnn_net = nn.RNN(
                    input_size=self.disease_num + self.symptom_num,
                    hidden_size=self.symptom_num,
                    num_layers=1,
                    batch_first=True,
                    nonlinearity="tanh",
                )
            else: # self.feature_model == 2 or 3 (use MLP)
                self.mlp_net = Net(
                    layer_num=2,
                    state_shape=tuple(
                        [
                            self.disease_num + self.symptom_num,
                        ]
                    ),
                    action_shape=self.symptom_num,
                    softmax=True,
                )

    def forward(
        self,
        s: Union[np.ndarray, torch.Tensor],
        state: Optional[Any] = None,
        info: Dict[str, Any] = {},
    ) -> Tuple[torch.Tensor, Any]:
        r"""Mapping: s -> Q(s, \*)."""
        s = to_torch(s, device=self.device, dtype=torch.float32)
        disease_probs = self.preprocess(s)  # bs * disease_num
        disease_probs = torch.clamp(disease_probs, 0.0, 1.0)
        if self.feature_model <= 1:  # only use transform matrix | 只使用transformatrix
            logits = torch.max(
                disease_probs.unsqueeze(2) * self.conditional_probability_matrix, 1
            )[
                0
            ]  # bs * symptom_num
        elif self.feature_model == 2:  # only use MLP | 只使用MLP
            mlp_input = torch.cat((disease_probs, s[:, self.disease_num :]), -1)
            logits = self.mlp_net(mlp_input)[0]
        elif self.feature_model == 3:  # all 3 | 三个都使用
            trans_logits = torch.max(
                disease_probs.unsqueeze(2) * self.conditional_probability_matrix, 1
            )[0]
            mlp_input = torch.cat((disease_probs, s[:, self.disease_num :]), -1)
            mlp_logits = self.mlp_net(mlp_input)[0]
            logits = trans_logits + mlp_logits
        elif self.feature_model == 4: # use MLP to learn a beita
            mlp_input = torch.cat((disease_probs, s[:, self.disease_num :]), -1)
            beita = torch.sigmoid(self.mlp_net(mlp_input)[0])
            beita = beita.unsqueeze(1)
            trans = (
                self.conditional_probability_matrix * beita
                + (torch.ones_like(beita) - beita) * self.mutual_information_matrix
            )
            logits = torch.max(disease_probs.unsqueeze(2) * trans, 1)[
                0
            ]  # bs * symptom_num
        elif self.feature_model == 5: # use simple weighted average
            logits = (
                self.conditional_matrix_weight
                * torch.max(
                    disease_probs.unsqueeze(2) * self.conditional_probability_matrix, 1
                )[0]
                + self.mutual_matrix_weight
                * torch.max(
                    disease_probs.unsqueeze(2) * self.mutual_information_matrix, 1
                )[0]
            )
        elif self.feature_model == 6: # use a RNN instead of an MLP
            rnn_input = torch.cat((disease_probs, s[:, self.disease_num :]), -1).unsqueeze(1)
            rnn_output, _ = self.rnn_net(rnn_input)
            beita = torch.sigmoid(rnn_output.squeeze(1))
            beita = beita.unsqueeze(1)
            trans = (
                self.conditional_probability_matrix * beita
                + (torch.ones_like(beita) - beita) * self.mutual_information_matrix
            )
            logits = torch.max(disease_probs.unsqueeze(2) * trans, 1)[0]
        else:
            print("Error - feature_model not defined")
            raise FeatureModelNotSupportedError

        if "history" in info.keys():
            history = torch.tensor(info["history"][:, self.disease_num :])
            mask = torch.ones_like(history) - history
        else:
            mask = torch.ones_like(logits)

        logits = logits * mask

        disease_num = disease_probs.size(-1)
        logits = torch.cat((disease_probs.float(), logits.float()), 1)
        for k in range(disease_probs.size(0)):
            if (
                (torch.max(disease_probs[k]) >= self.threshold and not self.training)
                or mask[k].sum() == 0
                or logits[k][self.disease_num :].sum() == 0.0
                or info.get("turn", np.zeros(disease_probs.size(0)))[k] >= self.max_turn
            ):
                logits[k][disease_num:] = torch.zeros(self.symptom_num)
            else:
                logits[k][:disease_num] = torch.zeros(disease_num)

        logits = to_torch(logits, device=self.device, dtype=torch.float32)
        return logits, state
