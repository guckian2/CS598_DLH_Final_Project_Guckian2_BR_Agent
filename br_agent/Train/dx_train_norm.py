import copy
import os
from multiprocessing import Process, cpu_count, Lock
import numpy as np
import pickle
from pgmpy.models import BayesianModel
import pprint
import random
import sys
from tianshou.policy import A2CPolicy
from tianshou.env import SubprocVectorEnv
from tianshou.trainer import onpolicy_trainer
from tianshou.data import Collector, ReplayBuffer
from tianshou.utils.net.discrete import Critic
from tianshou.utils.net.common import Net
import time
import torch
from torch.utils.tensorboard import SummaryWriter
from torch import nn
import time

sys.path.append("../")

from a2c.A2C import MyA2CPolicy
from a2c.Collect import MyCollector
from a2c.Policy import Myonpolicy_trainer
from a2c.Utils import *
from dataset.DataReader import get_dataset_information, load_disease_sym_pk
from Environment.MaskEnvrionment import MedicalEnvrionment
from NiceBayesian.ActorNet import MyActor
from NiceBayesian.GradBayesv import GradBayesModel
from utils.utils import overwrite_db
from utils.translator import translate
from utils.args import get_args


def save_fn(policy, save_model="./model/dxy/"):
    if not os.path.exists(save_model):
        os.makedirs(save_model)
    torch.save(policy, os.path.join(save_model, "policy_norm.pth"))


def get_data(args):
    slot_set = []
    with open("../dataset/dxy/dx_slot_set.txt", "r", encoding="utf-8") as f:
        for line in f.readlines():
            translation = translate(line.strip())
            slot_set.append(translation)

    (
        goals,
        total_disease_dict,
        total_sym_dict,
        bayes_data,
        bayes_graph,
        test_samples,
        test_ans,
    ) = get_dataset_information(
        data_path="../dataset/dxy/dxy_addnlu_goals_english.pk",
        disease_path="../dataset/dxy/dx_disease.txt",
        symptom_path="../dataset/dxy/dx_symptom.txt",
    )

    mutual_information = load_disease_sym_pk(
        "../dataset/dxy/dx_mutual_information_norm.pk"
    )
    conditional_probability_matrix = load_disease_sym_pk("../dataset/dxy/dx_relation_norm.pk")

    return (
        slot_set,
        goals,
        total_disease_dict,
        total_sym_dict,
        bayes_data,
        bayes_graph,
        test_samples,
        test_ans,
        mutual_information,
        conditional_probability_matrix,
    )


def test_a2c(cli=True, **kwargs):
    args = get_args(cli, **kwargs)

    if args.overwrite_db:
        overwrite_db()

    (
        slot_set,
        goals,
        total_disease_dict,
        total_sym_dict,
        bayes_data,
        bayes_graph,
        test_samples,
        test_ans,
        mutual_information,
        conditional_probability_matrix,
    ) = get_data(args)

    print("mutual_information: ", mutual_information.shape)
    print("conditional_probability_matrix: ", conditional_probability_matrix.shape)

    env = MedicalEnvrionment(slot_set, goals["test"])

    # Box has no attr "n", should think about removing the other side of "or"
    args.state_shape = env.observation_space.shape or env.observation_space.n

    # Discrete.shape returns (), should think about removing the other side of "or"
    args.action_shape = env.action_space.shape or env.action_space.n

    train_envs = SubprocVectorEnv(
        [
            lambda: MedicalEnvrionment(
                slot_set,
                goals["train"],
                max_turn=args.max_episode_steps,
                flag="train",
                disease_num=len(total_disease_dict),
            )
            for _ in range(args.training_num)
        ]
    )

    test_envs = SubprocVectorEnv(
        [
            lambda: MedicalEnvrionment(
                slot_set,
                goals["test"],
                max_turn=args.max_episode_steps,
                flag="test",
                disease_num=len(total_disease_dict),
            )
            for _ in range(args.test_num)
        ]
    )

    # seed
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    train_envs.seed(args.seed)
    test_envs.seed(args.seed)
    random.seed(args.seed)

    print("transfor_grad", args.transfor_grad)

    bayes_net = GradBayesModel(
        bayes_graph=bayes_graph,
        train_sample_data=bayes_data,
        disease_num=len(total_disease_dict),
        test_samples=test_samples,
        test_ans=test_ans,
    )

    net = Net(args.layer_num, args.state_shape, device=args.device)

    actor = MyActor(
        args,
        bayes_net,
        args.action_shape,
        conditional_probability_matrix=conditional_probability_matrix,
        mutual_information_matrix=mutual_information,
    ).to(args.device)

    critic = Critic(net).to(args.device)

    conv5_params = list(map(id, actor.preprocess.parameters()))
    base_params = filter(lambda p: id(p) not in conv5_params, actor.parameters())

    optim = torch.optim.Adam(
        [
            {"params": list(base_params) + list(critic.parameters())},
            {"params": list(actor.preprocess.parameters()), "lr": args.lr2},
        ],
        lr=args.lr,
    )

    dist = torch.distributions.Categorical
    policy = MyA2CPolicy(
        actor,
        critic,
        optim,
        dist,
        args.gamma,
        vf_coef=args.vf_coef,
        ent_coef=args.ent_coef,
        max_grad_norm=args.max_grad_norm,
    )

    # collector
    train_collector = MyCollector(policy, train_envs, ReplayBuffer(args.buffer_size))
    test_collector = MyCollector(policy, test_envs)

    # log
    path = "#".join(["feature", "with_cof_" + str(args.vf_coef), "lr:" + str(args.lr)])
    if args.time_name is None:
        time_name = time.strftime("%Y-%m-%d-%H_%M_%S", time.localtime())
        # time_name = path
    else:
        time_name = args.time_name
    writer = SummaryWriter(os.path.join(args.logdir, args.logpath + time_name))

    result = Myonpolicy_trainer(
        policy,
        train_collector,
        test_collector,
        args.epoch,
        args.step_per_epoch,
        args.collect_per_step,
        args.repeat_per_collect,
        len(goals["test"]),
        args.batch_size,
        save_fn=save_fn,
        writer=writer,
        verbose=True,
        test_probs=False,
    )
    path = (
        path
        + "    "
        + str(result["best_rate"])
        + "   mate_num_"
        + str(result["best_mate_num"])
        + "   best_len_"
        + str(result["best_len"])
    )
    return result


if __name__ == "__main__":
    result = test_a2c()
