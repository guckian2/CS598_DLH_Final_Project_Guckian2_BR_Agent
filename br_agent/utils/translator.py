from json import loads
from os import environ
from openai import api_key, ChatCompletion
from googletrans import Translator
from regex import search
from sqlite3 import connect
from time import sleep

from utils.utils import write_to_db, get_english_translation
from exceptions.exceptions import FailedToTranslateError

api_key = environ["OPENAI_API_KEY"]


def is_chinese_text(data):
    if isinstance(data, str):
        return True if search(r"\p{Han}", data) else False
    return False


def parse_result(response):
    result = (
        response["choices"][0]["message"]["content"].replace('"', "").replace(".", "")
    )
    result = result.replace("translates to ", "").replace(" in English", "")
    return result.strip().lower()


def translate(text):
    if not is_chinese_text(text):
        return text
    translation = None
    try:
        translation = get_english_translation(text)
    except:
        message = """
        I will give you a key from a Python dictionary that is in Chinese,
        you will translate the key to English, and answer with only the English translation.
        Do not respond with the original Chinese characters or any explanations.
        Keep the meaning the same, reply with only the English translation, and nothing else.
        The first key is '咳嗽'.
      """
        response = ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=[
                {"role": "system", "content": "You are an English translator"},
                {"role": "user", "content": message},
                {"role": "assistant", "content": "cough"},
                {"role": "user", "content": text},
            ],
        )

        translation = parse_result(response)
        record = (translation, text)  # english, chinese
        write_to_db(record)
        print(record)

    if translation is None:
        raise FailedToTranslateError(f"Couldn't translate: {text}")

    return translation
