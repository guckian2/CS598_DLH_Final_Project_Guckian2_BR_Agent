from sqlite3 import connect

from exceptions.exceptions import FailedToTranslateError

DATABASE_PATH = "../data/database.db"


def execute_query(query, data=()):
    con = connect(DATABASE_PATH)
    cur = con.cursor()
    res = cur.execute(query, data).fetchall()
    con.commit()
    con.close()
    return res

def write_to_db(record):
    execute_query(query="INSERT INTO translations VALUES(?, ?)", data=record)


def overwrite_db():
    execute_query("DROP TABLE IF EXISTS translations")
    execute_query("CREATE TABLE IF NOT EXISTS translations(english TEXT, chinese TEXT PRIMARY KEY)")


def get_english_translation(chinese_word):
    res = execute_query(query="SELECT english, chinese FROM translations WHERE chinese = ?", data=(chinese_word,))
    if len(res) > 0:
        return res[0][0]
    else:
        raise FailedToTranslateError(f"Didn't find translation for {chinese_word}")
