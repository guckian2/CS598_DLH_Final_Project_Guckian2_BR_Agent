import argparse


class Args:
    def __init__(
        self,
        seed=1626,
        buffer_size=20000,
        lr=1e-3,
        lr2=1e-3,
        gamma=0.9,
        epoch=30,
        step_per_epoch=16,
        collect_per_step=64,
        repeat_per_collect=1,
        batch_size=64,
        layer_num=3,
        training_num=16,
        test_num=1,
        logdir="log",
        render=0.0,
        device="cpu",
        vf_coef=1.0,
        ent_coef=0.001,
        max_grad_norm=None,
        max_episode_steps=22,
        logpath="log/",
        time_name=None,
        transfor_grad=False,
        feature_model=4,
        var_model=0,
        random_rate=1.0,
        prob_threshold=0.95,
        save_model=True,
        overwrite_db=False,
        max_turn=22,
        conditional_matrix_weight=0.5,
        mutual_matrix_weight=0.5,
    ):
        self.seed = seed
        self.buffer_size = buffer_size
        self.lr = lr
        self.lr2 = lr2
        self.gamma = gamma
        self.epoch = epoch
        self.step_per_epoch = step_per_epoch
        self.collect_per_step = collect_per_step
        self.repeat_per_collect = repeat_per_collect
        self.batch_size = batch_size
        self.layer_num = layer_num
        self.training_num = training_num
        self.test_num = test_num
        self.logdir = logdir
        self.render = render
        self.device = device
        self.vf_coef = vf_coef
        self.ent_coef = ent_coef
        self.max_grad_norm = max_grad_norm
        self.max_episode_steps = max_episode_steps
        self.logpath = logpath
        self.time_name = time_name
        self.transfor_grad = transfor_grad
        self.feature_model = feature_model
        self.var_model = var_model
        self.random_rate = random_rate
        self.prob_threshold = prob_threshold
        self.save_model = save_model
        self.overwrite_db = overwrite_db
        self.max_turn = max_turn
        self.conditional_matrix_weight = conditional_matrix_weight
        self.mutual_matrix_weight = mutual_matrix_weight


def get_args(cli, **kwargs):
    if cli:
        parser = argparse.ArgumentParser()
        parser.add_argument("--seed", type=int, default=1626)
        parser.add_argument("--buffer-size", type=int, default=20000)
        parser.add_argument("--lr", type=float, default=1e-3)
        parser.add_argument("--lr2", type=float, default=1e-3)
        parser.add_argument("--gamma", type=float, default=0.9)
        parser.add_argument("--epoch", type=int, default=30)
        parser.add_argument("--step-per-epoch", type=int, default=16)
        parser.add_argument("--collect-per-step", type=int, default=64)
        parser.add_argument("--repeat-per-collect", type=int, default=1)
        parser.add_argument("--batch-size", type=int, default=64)
        parser.add_argument("--layer-num", type=int, default=3)
        parser.add_argument("--training-num", type=int, default=16)
        parser.add_argument("--test-num", type=int, default=1)
        parser.add_argument("--logdir", type=str, default="log")
        parser.add_argument("--render", type=float, default=0.0)
        parser.add_argument("--device", type=str, default="cpu")

        # a2c special
        parser.add_argument("--vf-coef", type=float, default=1.0)  # 0.5 -- 0.9  0.1
        parser.add_argument("--ent-coef", type=float, default=0.001)
        parser.add_argument("--max-grad-norm", type=float, default=None)
        parser.add_argument("--max_episode_steps", type=int, default=22)
        parser.add_argument("--logpath", type=str, default="log/")
        parser.add_argument("--time_name", type=str, default=None)

        # my setting
        parser.add_argument("--transfor_grad", type=bool, default=False)
        parser.add_argument("--feature_model", type=int, default=4)
        parser.add_argument("--var_model", type=int, default=0)
        parser.add_argument("--random_rate", type=float, default=1.0)
        parser.add_argument("--max_turn", type=int, default=22)
        parser.add_argument("--prob_threshold", type=float, default=0.95)
        parser.add_argument("--save_model", type=bool, default=True)
        parser.add_argument("--overwrite-db", action="store_true", default=False)
        parser.add_argument("--conditional-matrix-weight", type=float, default=0.5)
        parser.add_argument("--mutual-matrix-weight", type=float, default=0.5)
        return parser.parse_args()

    else:
        return Args(**kwargs)
