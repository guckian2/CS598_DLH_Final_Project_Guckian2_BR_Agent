class FailedToTranslateError(Exception):
    pass

class FeatureModelNotSupportedError(Exception):
    pass
