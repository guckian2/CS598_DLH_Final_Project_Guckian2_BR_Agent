# BR-Agent

*This is a purely academic repository that attempts to replicate the experiements conducted in the following IJCAI'2022 paper: ["'My nose is running.''Are you also coughing?': Building A Medical Diagnosis Agent with Interpretable Inquiry Logics"](https://arxiv.org/abs/2204.13953).*

*This repository is a fork of [the authors' repository](https://github.com/lwgkzl/BR-Agent/tree/main) that they published alongside their paper. Due to privacy concerns, this repository could not be formally forked; therefore, I claim none of this work as my own as it belongs to the original authors. I am merely attempting to replicate their results for purely academic purposes.*

## Installation：

Run the following from the project root directory:
```
python3.10 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
```

## Running:

Run the following from the project root directory:
```
cd br_agent/Train/
python dx_train_norm.py
```

This will run the model with the default parameters which are the same parameters used to train and test the "baseline" model. "Baseline" in this case meaning the model that reproduces the authors' claim that they are able to achieve 84.6% diagnosis accuracy on the Dxy dataset. In order to run the experiments that I describe in the paper, you can use these options:

**Simple 50/50 weighted average of the conditional probability and mutual information matrix instead of MLP for the neural logic switcher:**
```
python dx_train_norm.py --feature_model=5
```

**Simple 25/75 weighted average of the conditional probability and mutual information matrix instead of MLP for the neural logic switcher (this is the one that performs better than the "baseline"):**
```
python dx_train_norm.py --feature_model=5 --conditional-matrix-weight=0.25, mutual-matrix-weight=0.75
```

**Simple 75/25 weighted average of the conditional probability and mutual information matrix instead of MLP for the neural logic switcher:**
```
python dx_train_norm.py --feature_model=5 --conditional-matrix-weight=0.75, mutual-matrix-weight=0.25
```

**Recurrent Neural Network instead of MLP for the neural logic switcher:**
```
python dx_train_norm.py --feature_model=6
```

## Notebook
After running the Installation steps, go into the `notebooks` directory and run `jupyter notebook`. The Jupyter menu should appear in your browser. From here, open `Spring 2023 CS598 DLH Final Project DRAFT Notebook.ipynb` to review my notebook.

## Results

After training the model, the command-line output will print the results to the console in a format similar to the following:
```
Epoch #30: test_reward: 22.528846 ± 27.885396, best_reward: 23.105769 ± 28.641994  hit_rate: 0.8461538461538461:.3f mate_num:  94.0 avg_len:  12.317307692307692
  best_rate: 0.8461538461538461:.3f  best_mate_num: 94.0:.3f  best_len: 12.317307692307692:.3f  in: #22:.3f
```
The `best_rate` reported above reflects the diagnosis accuracy of the BR-Agent. In my experiments, this rate reproduces the original authors' results within 1% accuracy as they originally reported a diagnosis accuracy of 84.6%.

The Jupyter notebook also has an interactive graph at the end that provides an overview of the diagnosis accuracy results of each model that I've trained.
